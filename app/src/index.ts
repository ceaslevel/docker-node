import express, { Request, Response } from 'express'
import * as dotenv from 'dotenv'
import json from 'body-parser'
import { Pool } from 'pg'

dotenv.config({path: '/web/.env'})

const app: express.Application = express();
const node_port = process.env.NODE_PORT || 3000;

app.set("port", node_port)

const pool = new Pool()

app.get('/hellodb', (req, res) => {
	pool.connect((err, client, release) => {
	  if (err) {
	    return console.error('Error acquiring client', err.stack)
	  }
	  const start = Date.now()
	  const sql = `SELECT NOW()`
	  client.query(sql, (err, result) => {
	  	const duration = Date.now() - start
	  	console.log('executed query', { sql, duration, rows: result.rowCount })
	    release()
	    if (err) {
	      return console.error('Error executing query', err.stack)
	    }
	    console.log({ result: result.rows })
	    res.json(result.rows)
	  })
	})
})

app.listen(app.get("port"), () => {
	console.log(`Listening on ${node_port}`);
});

export default app;


For development:

```bash
cp .env.sample .env
make dev
```
> Remember to  change the project names in the environment file!

Test the db connection: go to `localhost:$NODE_PORT/hellodb`. Expected output on the server console:
```
   | Listening on 3000
   | executed query { sql: 'SELECT NOW()', duration: 4, rows: 1 }
   | { result: [ { now: 2021-07-25T18:48:25.147Z } ] }
```


